﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    Animator animator;
    [SerializeField]
    PlayerController playerController;
    [SerializeField]
    AttackController attackController;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat(Animator.StringToHash("SpeedPercent"), playerController.GetSpeedPercent());
        animator.SetBool(Animator.StringToHash("Attack"), attackController.IsAttacking());
        animator.SetBool(Animator.StringToHash("Roll"), Input.GetMouseButton(1));

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Movement"))
        {
            playerController.speedMultipier = 1f;
        }
        else
        {
            playerController.speedMultipier =0.1f;
        }
    }
}
