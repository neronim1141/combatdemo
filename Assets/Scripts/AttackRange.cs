﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRange : MonoBehaviour
{
    BoxCollider collider;
    public float range = 0.5f;
    List<Health> targets = new List<Health>();
    public List<Health> InRange
    {
        get
        {
            return targets;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        collider = GetComponent<BoxCollider>();
        collider.size = new Vector3(1.5f, 1, range);
    }

    void OnTriggerEnter(Collider collision)
    {
        Health h = collision.gameObject.GetComponent<Health>();
        Debug.Log("some");
        if (h != null)
        {
            Debug.Log("health");
            targets.Add(h);

        }

    }
    void OnTriggerExit(Collider collision)
    {
        Health h = collision.gameObject.GetComponent<Health>();
        if (h != null)
        {
            if (targets.Contains(h))
                targets.Remove(h);
        }

    }
}
