﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThridPersonCamera : MonoBehaviour
{
    public float sensitivity = 10;
    public float zoomSpeed = 10;
    public Transform target;
    public float distanceToTarget=2;
    public bool lockCursor=false;
    float yaw;
    float pitch;
    Vector2 pitchMinMax = new Vector2(-40, 85);
    Vector2 zoomMinMax = new Vector2(2, 10);
    public float rotationSmoothTime = .12f;
    Vector3 rotationSmoothVelocity;
    Vector3 currentRotation;
    // Start is called before the first frame update
    void Start()
    {
        if (lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        yaw += Input.GetAxis("Mouse X")* sensitivity;
        pitch -= Input.GetAxis("Mouse Y") * sensitivity;
        pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);

        distanceToTarget -= Input.mouseScrollDelta.y*Time.deltaTime * zoomSpeed;
        distanceToTarget = Mathf.Clamp(distanceToTarget, zoomMinMax.x, zoomMinMax.y);


        currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(pitch, yaw), ref rotationSmoothVelocity, rotationSmoothTime);
        transform.eulerAngles = currentRotation;

        transform.position = target.position - transform.forward * distanceToTarget;

    }
}
