﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResourcePool
{
   void Drop(float amount);
   void Regen(float amount);
}
