﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    [SerializeField]
    CharacterStats stats;

    const float MAX_CHARGING_TIME=1f;
    const float CHARGED_ATTACK_THRESHOLD = 0.5f;
    const float ATTACK_COOLDOWN = 1f;

    bool isAttacking = false;
    float chargeTime = 0;
    public float fromLastAttack = 0;
    [SerializeField]
    AttackRange attackRange;


    // Update is called once per frame
    void Update()
    {
        bool attackEnd = Input.GetMouseButtonUp(0);
        if (Input.GetMouseButtonDown(0))
        {
            if (fromLastAttack > ATTACK_COOLDOWN)
            {
                isAttacking = true;
                fromLastAttack = 0;
            }
        }
        if(chargeTime> MAX_CHARGING_TIME)
        {
            attackEnd = true;
        }
        //attack was released;
        if (isAttacking && attackEnd)
        {
            if (chargeTime > 0)
            {
                bool isHeavy = false;
                if (chargeTime > CHARGED_ATTACK_THRESHOLD)
                {
                    isHeavy = true;

                }
                foreach (Health h in attackRange.InRange)
                {
                    h.Drop(stats.strength.Value * (isHeavy ? 1.2f : 1));
                }
            }
            chargeTime = 0;
            isAttacking = false;

        }
        if (isAttacking)
        {
            chargeTime += Time.deltaTime;
        }
        else
        {
            fromLastAttack += Time.deltaTime;
        }
    }
    public bool IsAttacking()
    {
        return isAttacking;
    }
}
