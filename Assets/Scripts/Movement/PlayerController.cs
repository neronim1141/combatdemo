﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    CharacterStats stats;
    public float walkSpeed=5;
    public float runSpeed=10;
    public float acceleration = 10;
    public float rotation = 10;
    float currentSpeed = 0f;
    public bool hasControll = true;
    Transform cameraT;
    public float turnSmoothTime = 0.2f;
    float turnSmoothVelocity;
    public float speedMultipier = 1;
    // Start is called before the first frame update
    void Start()
    {
        cameraT = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
        
            Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            Vector2 inputDir = input.normalized;


            bool running = Input.GetKey(KeyCode.LeftShift);
            CalculateSpeed(input != Vector2.zero, running);

            inputDir *= currentSpeed;
            if (input != Vector2.zero)
            {
                Rotate(inputDir);

            }
            Move();
        
    }
    void CalculateSpeed(bool input,bool running)
    {
        if (input)
        {
            currentSpeed = Mathf.MoveTowards(currentSpeed, (running ? runSpeed : walkSpeed), acceleration * Time.deltaTime);

        }
        else
        {
            currentSpeed = Mathf.MoveTowards(currentSpeed, 0, acceleration * 5 * Time.deltaTime);
        }
    }
    void Rotate(Vector2 inputDir)
    {
        float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + cameraT.eulerAngles.y;
        transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime);

    }
    void Move()
    {
        transform.Translate(transform.forward * currentSpeed*speedMultipier * Time.deltaTime, Space.World);

    }
    public float GetSpeedPercent()
    {
        return currentSpeed / runSpeed;
    }
}
