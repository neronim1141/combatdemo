﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public CharacterStat strength;
    public CharacterStat dexterity;
    public CharacterStat will;
    public CharacterStat constitution;


}
