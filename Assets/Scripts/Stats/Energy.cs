﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : ResourcePool,IResourcePool
{
    const int MAX_ENERGY = 100;

    // Start is called before the first frame update
    void Start()
    {
        value = MAX_ENERGY;
        UIBar.SetPercent(value / MAX_ENERGY);
    }

    // Update is called once per frame
    void Update()
    {
        Regen(stats.will.Value * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.LeftShift))
        {
            Drop(10);
        }
    }

    public void Drop(float amount)
    {
        value = Mathf.Max(value - amount, 0);

        UIBar.SetPercent(value / MAX_ENERGY);
    }

    public void Regen(float amount)
    {
        value = Mathf.Min(value + amount, MAX_ENERGY);

       UIBar.SetPercent(value / MAX_ENERGY);
    }
}
