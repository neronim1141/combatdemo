﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : ResourcePool, IResourcePool
{


    // Start is called before the first frame update
    void Start()
    {
        value = stats.constitution.Value;
        UIBar.SetPercent(value / stats.constitution.Value);
    }

    // Update is called once per frame
    void Update()
    {
        Regen(stats.will.Value * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.Space) && !Input.GetKey(KeyCode.LeftShift))
        {
            Drop(10);
        }
       
    }
    public void Drop(float amount)
    {
        value = Mathf.Max(value - amount, 0);

        UIBar.SetPercent(value / stats.constitution.Value);
    }

    public void Regen(float amount)
    {
        value = Mathf.Min(value + amount, stats.constitution.Value);

        UIBar.SetPercent(value / stats.constitution.Value);
    }
}
