﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ResourcePool : MonoBehaviour
{ 
    [SerializeField]
    protected CharacterStats stats;
    [SerializeField]
    protected float value;
    [SerializeField]
    protected Indicator UIBar;


}

