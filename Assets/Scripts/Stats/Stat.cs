﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct CharacterStat
{
    [SerializeField]

    int points;
    [SerializeField]

    Stat stat;
    public float Value { get
        {
            return stat.Value(points);
        }
    }

    public CharacterStat(int points, Stat stat)
    {
        this.points = points;
        this.stat = stat;
    }

    public void Add()
    {
        points++;
    }
}
[CreateAssetMenu()]
public class Stat :ScriptableObject
{
    [SerializeField]
    float maxValue;
    [SerializeField]
    float minValue;
    [SerializeField]
    float scalar;

    public Stat(float minValue, float maxValue, float scalar)
    {
        if (scalar <= 0)
        {
            throw new System.Exception("Scalar should be greater than 0");
        }
        this.maxValue = maxValue;
        this.minValue = minValue;
        this.scalar = scalar;
    }
    public float Value(int points)
    {
        return Mathf.Lerp(minValue, maxValue, -(scalar / (points + scalar)) + 1f);
    }
}
