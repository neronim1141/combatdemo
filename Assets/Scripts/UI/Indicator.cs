﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Indicator : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public Image bar;
    void Start()
    {
        
    }

   public void SetPercent(float percent)
    {
        bar.fillAmount = percent;
    }
}
