﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class StatTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void LowStat()
        {
            CharacterStat stat = new CharacterStat(0, new Stat(10, 1000, 20));
            Assert.AreEqual(stat.Value, 10);
        }
        [Test]
        public void HalfWayStat()
        {
            CharacterStat stat = new CharacterStat(20, new Stat(10, 1000, 20));
            Assert.AreEqual(stat.Value, 505);
        }


    }
}
